import Constants._
import org.apache.spark.SparkConf
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

case class Foo(s: String) extends AnyVal

case class ServiceConf(foo: Foo)

object Soccer {

  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName(APP_NAME).setMaster(MASTER)
    val spark: SparkSession = SparkSession.builder().config(conf).enableHiveSupport().getOrCreate()
    import spark.implicits._

    /***
     * Q1
     */

    val soccerFile = FILE_LOC
    val soccerData = spark.read.format("csv").option("header", "true").load(soccerFile)

    /***
     * Q1.5
     */

    val spData = soccerData.filter($"Div" === "SP1")

    /***
     * Q2
     */

    val droppedData = spData.drop(COLS_TO_DROP: _*)

    /***
     * Q3
     */

    val columnsMap = spark.read.csv(COLUMNS_FILE).collect().map(row => (row.getString(0), row.getString(1)))
    val renamedData = columnsMap.foldLeft(droppedData)((acc, ca) => ca match { case (origName, newName) => acc.withColumnRenamed(origName, newName) })

    /***
     * Q4
     */

    val convertedData = renamedData.withColumn(FINAL_RESULT,
      when(col(FINAL_RESULT).equalTo("A"), GameResult.Away.toString)
      .when(col(FINAL_RESULT).equalTo("H"), GameResult.Home.toString)
      .otherwise(GameResult.Draw.toString))

    /***
     * Q5
     */

    val homeWins = convertedData.filter(col(FINAL_RESULT) === GameResult.Home.toString)

    /***
     * Q6
     */

    val testFormat = convertedData.withColumn(DATE, date_format(to_date(col(DATE), "yyyy-MM-dd"), "MM/dd/yy"))

    val formattedData = convertedData

    /***
     * Q7
     */

    val unpredictedGames = formattedData.filter(
      (col(HOME_SHOTS) > col(AWAY_SHOTS) && col(FINAL_RESULT) === GameResult.Away.toString) ||
        (col(HOME_SHOTS) < col(AWAY_SHOTS) && col(FINAL_RESULT) === GameResult.Home.toString))
      .withColumn("home_shots_precision", col("home_shots_on_target") / col(HOME_SHOTS))
      .withColumn("away_shots_precision", col("away_shots_on_target") / col(AWAY_SHOTS))

    /***
     * Q8
     */

    val cleanWins = formattedData.filter(
      (col(AWAY_GOALS) === "0.0" && col(FINAL_RESULT) === GameResult.Home.toString) ||
        (col(HOME_GOALS) === "0.0" && col(FINAL_RESULT) === GameResult.Away.toString))

    /***
     * Q9
     */

    val mostGoals = formattedData.withColumn("total_goals", col(HOME_GOALS) + col(AWAY_GOALS))
      .orderBy(desc("total_goals"))
      .limit(1)

    /***
     * Q10
     */

    val interestingData = formattedData.withColumn("interest_level",
      when(((col(HOME) isin (INTERESTING_TEAMS:_*)) && (col(AWAY) isin (INTERESTING_TEAMS:_*))) ||
        (col(HOME_GOALS) + col(AWAY_GOALS) > 5), "interesting")
        .when((col(HOME) isin (INTERESTING_TEAMS:_*)) || (col(AWAY) isin (INTERESTING_TEAMS:_*)) ||
          (col(HOME_GOALS) + col(AWAY_GOALS) > 2), "medium")
        .otherwise("not interesting"))

    /***
     * Q11
     */

    val worstTeam = interestingData.filter($"interest_level" === "interesting")
      .filter(col(FINAL_RESULT) =!= GameResult.Draw.toString)
      .filter(((col(FINAL_RESULT) === GameResult.Away.toString) && (col(HOME) isin (INTERESTING_TEAMS:_*))) ||
        ((col(FINAL_RESULT) === GameResult.Home.toString) && (col(AWAY) isin (INTERESTING_TEAMS:_*))))
      .withColumn("loser",
        when(col(FINAL_RESULT) === GameResult.Away.toString, col(HOME))
          .when(col(FINAL_RESULT) === GameResult.Home.toString, col(AWAY)))
      .groupBy("loser")
      .count()
      .orderBy(desc("count"))
      .limit(1)

    /***
     * Q12
     */

    val (redFactor, yellowFactor, faulFactor) = (3, 2, 1)

    val homeData = formattedData.withColumn("Team", col(HOME))
      .withColumn("Aggression", col("home_red_cards") * redFactor +
        col("home_yellow_cards") * yellowFactor +
        col("home_fauls") * faulFactor)
    val awayData = formattedData.withColumn("Team", col(AWAY))
      .withColumn("Aggression", col("away_red_cards") * redFactor +
        col("away_yellow_cards") * yellowFactor +
        col("away_fauls") * faulFactor)

    homeData.union(awayData)
      .groupBy("Team")
      .agg(sum("Aggression") as "Aggression")
      .orderBy(desc("Aggression")).limit(1)

    /***
     * Q13
     */

    val columns = formattedData.columns
    val regex = """.+[HDA]"""
    val selection: Array[String] = columns.filter(c => c.matches(regex))

    val bestAgency = formattedData.map(game => {
      val winner = GameResult.withName(game.getAs[String](FINAL_RESULT))
      val winnerChar = winner match {
        case GameResult.Home => "H"
        case GameResult.Away => "A"
        case GameResult.Draw => "D"
      }

      selection.filter(c => c.endsWith(winnerChar)).reduce((c1, c2) => {
        val odd1 = game.getAs[String](c1)
        val odd2 = game.getAs[String](c2)
        if (odd1 == null) c2 else if (odd2 == null) c1
          else if (odd1.toDouble >= odd2.toDouble) c1 else c2
      })
    })

    /***
     * Q14
     */

    def calcTable(date: String): DataFrame = {
      val games = formattedData.filter(col(DATE) < date)

      def finalizeStats(df: DataFrame): DataFrame = {
        df.withColumn("Draws", when(col(FINAL_RESULT) === GameResult.Draw.toString, 1).otherwise(0))
        .select("Team", "Points", "Wins", "Loses", "Draws", "Goals", "GoalsReceived")
      }

      val homeStats = finalizeStats(games.withColumn("Team", col(HOME))
        .withColumn("Points", when(col(FINAL_RESULT) === GameResult.Home.toString, 3).when(col(FINAL_RESULT) === GameResult.Draw.toString, 1).otherwise(0))
        .withColumn("Wins", when(col(FINAL_RESULT) === GameResult.Home.toString, 1).otherwise(0))
        .withColumn("Loses", when(col(FINAL_RESULT) === GameResult.Away.toString, 1).otherwise(0))
        .withColumn("Goals", col(HOME_GOALS))
        .withColumn("GoalsReceived", col(AWAY_GOALS)))

      val awayStats = finalizeStats(games.withColumn("Team", col(AWAY))
        .withColumn("Points", when(col(FINAL_RESULT) === GameResult.Away.toString, 3).when(col(FINAL_RESULT) === GameResult.Draw.toString, 1).otherwise(0))
        .withColumn("Wins", when(col(FINAL_RESULT) === GameResult.Away.toString, 1).otherwise(0))
        .withColumn("Loses", when(col(FINAL_RESULT) === GameResult.Home.toString, 1).otherwise(0))
        .withColumn("Goals", col(AWAY_GOALS))
        .withColumn("GoalsReceived", col(HOME_GOALS)))

      homeStats.union(awayStats).groupBy("Team")
        .agg(sum("Points") as "Points",
          sum("Wins") as "Wins",
          sum("Loses") as "Loses",
          sum("Draws") as "Draws",
          sum("Goals") as "Goals",
          sum("GoalsReceived") as "GoalsReceived")
        .orderBy(desc("Points"))
    }

    val date = "2010-12-28"
    val sampleTable = calcTable(date)

    /***
     * Q15
     */

    val leagueGames = formattedData.filter(col(DATE) < date)
    val lastTeam = sampleTable.orderBy(asc("Points")).first().getAs[String]("Team")

    val losingHomeTeams = leagueGames.filter(col(AWAY) === lastTeam && leagueGames(FINAL_RESULT) === GameResult.Away.toString)
      .select(col(HOME))
      .withColumnRenamed(HOME, "Team")
    val losingAwayTeams = leagueGames.filter(col(HOME) === lastTeam && leagueGames(FINAL_RESULT) === GameResult.Home.toString)
      .select(col(AWAY))
      .withColumnRenamed(AWAY, "Team")
    val losingTeams = losingHomeTeams.union(losingAwayTeams).distinct()

    /***
     * Q16
     */

    val homePoints = leagueGames.withColumn("Points", when(col(FINAL_RESULT) === GameResult.Home.toString, 3)
      .when(col(FINAL_RESULT) === GameResult.Draw.toString, 1).otherwise(0))
      .withColumn("Team", col(HOME))
      .withColumn("Opponent", col(AWAY))
    val awayPoints = leagueGames.withColumn("Points", when(col(FINAL_RESULT) === GameResult.Away.toString, 3)
      .when(col(FINAL_RESULT) === GameResult.Draw.toString, 1).otherwise(0))
      .withColumn("Team", col(AWAY))
      .withColumn("Opponent", col(HOME))
    val totalPoints = homePoints.union(awayPoints)

    val window = Window.partitionBy($"Team")

    val mostProductiveTeam = totalPoints.groupBy("Team", "Opponent").agg(sum(col("Points")) as "Points")
      .select('*, max('Points) over window as "MaxPoints")
      .filter(col("Points") === col("MaxPoints"))
      .dropDuplicates("Team")
      .drop("MaxPoints")
  }
}