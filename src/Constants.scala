
object Constants {

  val APP_NAME = "Soccer"
  val MASTER = "local[*]"
  val FILE_LOC = "D:\\Studies\\Spark\\european_soccer.csv"
  val COLUMNS_FILE = "D:\\Studies\\Spark\\soccer-columns.txt"

  val COLS_TO_DROP: Array[String] = Array("Div", "BblX2", "BbMxH", "BbAvH", "BbMxD", "BbAvD", "BbMxA", "B", "bAvA", "BbOU", "BbMx>2.5",
    "BbAv>2.5", "BbMx<2.5", "BbAv<2.5", "BbAH", "BbAHh", "BbMxAHH", "BbAvAHH", "BbMxAHA", "BbAvAHA", "PSCH", "PSCD",
    "PSCA")

  def INTERESTING_TEAMS: Array[String] = Array("Ath Bilbao", "Barcelona", "Real Madrid", "Sevilla")

  object GameResult extends Enumeration {
    type GameResult = Value
    val Home: Constants.GameResult.Value = Value("home")
    val Away: Constants.GameResult.Value = Value("away")
    val Draw: Constants.GameResult.Value = Value("draw")
  }

  val FINAL_RESULT = "final_time_result"
  val DATE = "Date"
  val HOME = "HomeTeam"
  val AWAY = "AwayTeam"
  val HOME_SHOTS = "home_shots"
  val AWAY_SHOTS = "away_shots"
  val HOME_GOALS = "final_time_home_goals"
  val AWAY_GOALS = "final_time_away_goals"
}
